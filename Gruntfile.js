module.exports = function(grunt){
	grunt.initConfig({
		copy:{
			public: {
				expand: true,
				cwd: 'public',
				// src: '**',
				src: ['*.html', '*.php', '*.pdf', '*.docx', '*.csv', '*.xls', '*.txt', '.htaccess','phpmailer/**','includes/**','components/**', 'assets/img/*','assets/img/*/**','assets/fonts/**','assets/fonts/**/*','assets/css/**/*', 'assets/js/**/*'], 
				dest: 'dist',
				dot: true
			}
		},

		clean: {
			dist: {
				src: 'dist'
			}
		},

		useminPrepare: {
			html: 'dist/**/*.php'
		},

		usemin: {
			html: 'dist/**/*.php'
		},

		imagemin: {
			public:{ 
				expand: true,
				cwd: 'dist/assets/img',
				src: '**/*.{png,jpg,gif}',
				dest: 'dist/assets/img'
			}
		},

		// rev: {
		// 	options: {
		// 		encoding: 'utf-8',
		// 		algorithm: 'md5',
		// 		lenght: 8
		// 	},			
		// 	imagens: {
		// 		src: ['dist/img/**/*.{png,jpg,gif}']
		// 	},
		// 	minificados:{
		// 		src: ['dist/js/**/*.min.js', 'dist/css/**/*.min.css']
		// 	}
		// },

		less: {
			compilar: {
				expand: true,
				cwd: 'public/assets/less',
				src: ['**/*.less'],
				dest: 'public/assets/css',
				ext: '.css'
			}
		},

		watch: {
			less: {
				options: {
					event: ['added', 'changed']
				},
				files: 'public/assets/less/**/*.less',
				tasks: 'less:compilar'
			}
		},

	});

	grunt.registerTask('dist',  ['clean','copy']);
	grunt.registerTask('minifica',  ['useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin', 'imagemin']);
	grunt.registerTask('default', ['dist', 'less', 'minifica']);

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

}