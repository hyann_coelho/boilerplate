<!-- build:js assets/js/main.min.js -->
<script type="text/javascript" src="assets/js/libs/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/libs/modernizr.js"></script>
<script type="text/javascript" src="assets/js/libs/jquery.bxslider-rahisified.min.js"></script>
<script type="text/javascript" src="assets/js/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/libs/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/libs/messages_pt_BR.js"></script>
<script type="text/javascript" src="assets/js/libs/jquery.mask.min.js"></script>
<script type="text/javascript" src="assets/js/libs/wow.min.js"></script>
<script type="text/javascript" src="assets/js/libs/smoothscroll.js"></script>
<script type="text/javascript" src="assets/js/libs/salvaweb.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<!-- endbuild -->