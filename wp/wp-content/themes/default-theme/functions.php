<?php

date_default_timezone_set("America/Sao_Paulo"); 
setlocale(LC_ALL, 'pt_BR');

// function set_html_content_type() {
//     return 'text/html';
// }
            
// add_filter( 'wp_mail_content_type', 'set_html_content_type' );

// Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
// remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

// Remove avisos de atualização do WordPress
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );

function arpen_setup() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-logo', array(
		// 'height'      => 240,
		// 'width'       => 240,
		// 'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	function html5blank_nav(){
		wp_nav_menu(
			array(
				'theme_location'  => 'header-menu',
				'menu'            => '',
				'container'       => 'nav',
				'container_class' => 'menu-{menu slug}-container',
				'container_id'    => '',
				'menu_class'      => 'menu',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul>%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			)
		);
	}

	register_nav_menus( array(
        'main_menu' => __( 'Menu Principal', '' ),
        // 'footer_menu' => __( 'Menu footer', '' ),
	));

	/*
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );
	*/

}
add_action( 'after_setup_theme', 'arpen_setup' );

function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


function b_widgets_init() {

    // footer widgets
    register_sidebar( array(
        'name'          => __( 'Footer Area 1', '' ),
        'id'            => 'footer1',
        'description'   => __( '', '' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="tl">',
        'after_title'   => '</h4>',
    ));
    // footer widgets
    register_sidebar( array(
        'name'          => __( 'Footer Area 2', '' ),
        'id'            => 'footer2',
        'description'   => __( '', '' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="tl">',
        'after_title'   => '</h4>',
    ));
    // footer widgets
    register_sidebar( array(
        'name'          => __( 'Footer Area 3', '' ),
        'id'            => 'footer3',
        'description'   => __( '', '' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="tl">',
        'after_title'   => '</h4>',
    ));
    // footer widgets
    register_sidebar( array(
        'name'          => __( 'Footer Area 4', '' ),
        'id'            => 'footer4',
        'description'   => __( '', '' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h4 class="tl">',
        'after_title'   => '</h4>',
    ));
}
add_action( 'widgets_init', 'b_widgets_init' );

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    // add_image_size( 'slide-home', 848, 506, array( 'center', 'center', true ));
    // add_image_size( 'adv', 250, 305, array( 'center', 'center', true ));
    // add_image_size( 'noticia', 490, 330, array( 'center', 'center', true ));
    // add_image_size( 'noticia-home', 700, 490, array( 'center', 'center', true ));
    // add_image_size( 'noticia-interna', 768, 430, array( 'center', 'center', true ));
	// add_image_size( 'news-thumbnail-medium', 770, 300, array( 'center', 'center', true ));
}


function hls_set_query() {
	$query = attribute_escape(get_search_query()); if(strlen($query) > 0){ 
		echo ' <script type="text/javascript"> var hls_query = "'.$query.'"; </script> '; 
	}
}

function hls_init_jquery() { 
	wp_enqueue_script('jquery'); 
}

add_action('init', 'hls_init_jquery'); add_action('wp_print_scripts', 'hls_set_query');


function format_the_excerpt($excerpt, $num) {
    $excerpt = explode(' ', $excerpt);
    $strExcerpt = "";
    $count=0;
    $endPoints = "";
    foreach ($excerpt as $word) {
        if($count < $num){
            $strExcerpt .= $word." ";
        }else{
            $endPoints="...";
            break;
        }
        $count++;
    }

    return trim($strExcerpt).$endPoints;
}

do_action('wp', 'format_the_excerpt');


// public function widget( $args, $instance ) {
//   $title = apply_filters( 'widget_title', $instance[ 'title' ] );
//   $blog_title = get_bloginfo( 'name' );
//   $tagline = get_bloginfo( 'description' );
//   echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title'];

//   echo '<p><strong>Site Name:</strong>'.$blog_title.'</p>';
//   echo '<p><strong>Tagline:</strong>'.$tagline.'</p>';

//   echo $args['after_widget'];
// }



// Verifica se não existe nenhuma função com o nome tp_count_post_views
if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () { 
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {
        
            // Precisamos da variável $post global para obter o ID do post
            global $post;
            
            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {
                
                // Cria a sessão do posts
                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;
            
                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );
                
                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    // Caso contrário, o valor atual + 1
                    $key_value += 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor
                
            } // Checa a sessão
            
        } // is_single
        
        return;
        
    }
    add_action( 'get_header', 'tp_count_post_views' );
}

/*
function load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => '2',
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );
    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
            <h2><?php the_title() ?></h2>
            <?php the_excerpt() ?>
        <?php endwhile ?>
        <?php
    endif;

    wp_die();
}
*/


?>