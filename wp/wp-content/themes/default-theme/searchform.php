<form action="<?php echo home_url( '/' ); ?>" role="search" method="get" >
	<div class="form-group submit-box-search">
		<input type="search" name="s" placeholder="Buscar" required class="form-control">
		<button type="submit" class="btn-submit"><i class="fa fa-search" aria-hidden="true"></i></button>
	</div>
</form>