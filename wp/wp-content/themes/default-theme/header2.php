<!DOCTYPE HTML>
<html lang="pt-br" class="no-js">
	<head>
    	<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
		<?php  if(!is_plugin_active( 'wordpress-seo/wp-seo.php' )){ ?>

		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' | '; } ?> <?php bloginfo('name'); ?></title>

		<?php } ?>

		<meta name="author" content="Wrapper Design">
		<meta name="URL" content="<?php global $wp; echo home_url( $wp->request ); ?>">
		<meta name="robots" content="index, follow">
		<meta name="reply-to" content="">
		<meta name="classification" content="Internet">
		<meta name="distribution" content="Global">
		<meta name="rating" content="Safe for Kids">
		<meta name="theme-color" content="#a4956c">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.min.css" media="screen">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/html5shiv.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/respond.min.js"></script>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/ie8-style.css" media="screen" />
		<![endif]-->

		<?php wp_head(); ?>

	</head>
<body <?php body_class(); ?>>

	<?php
		$logo = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
		// if(!empty($logo)){}
	?>
	<?php
        $defaults = array(
        	'theme_location'  => 'main_menu',
            'menu'            => 'Menu Principal',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'top-menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0
        );
    	wp_nav_menu( $defaults );
	?>