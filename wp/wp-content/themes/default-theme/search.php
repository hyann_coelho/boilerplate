<?php get_header(); ?>

<?php $s = trim($_GET['s']); ?>

<?php
	wp_reset_query();
	$query_args = array ( 
		'post_type' => array('post'),
		'posts_per_page' => -1,
		's' => $s,
		// 'paged' => get_query_var( 'paged' )
	);                    
	$wp_query = new WP_Query($query_args);
	if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts()) : $wp_query->the_post();  
		$id = get_the_ID();
		if ( has_post_thumbnail() ) {
			$post_img = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'thumbnail' );
			$post_img = $post_img['0'];
		}
?>

<?php unset($post_img); endwhile;  else: { } endif; ?>
<?php get_footer(); ?>

</body>
</html>