<?php
	get_header(); 
?>

<section class="wrapper sec-pad">
	<div class="container">
		<div class="row">
			<article class="col-xs-24 xs-text-center">
				<h2 class="tl-2 text-blue">Página não encontrada</h2>
				<p>Ops! Parece que o link que você tentou acessar não existe mais ou mudou de endereço.</p>
				<a href="<?php echo home_url(); ?>" class="btn-primary">Ir para a home</a>
			</article>
		</div>
	</div>
</section>

<?php get_footer(); ?>

</body>
</html>