<?php $server = $_SERVER['SERVER_NAME']; $endereco = $_SERVER ['REQUEST_URI']; ?>
<!DOCTYPE HTML>
<html lang="pt-br" class="no-js">
	<head>
    	<link href="assets/img/favicon.png" rel='shortcut icon'/>
    	<link href="assets/img/favicon.png" rel='icon'/>
    	<link href="assets/img/favicon.png" rel='apple-touch-icon'/>
    	<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<title><?php echo $page_title; ?></title>

		<meta name="description" content="">
		<meta name="author" content="Wrapper Design">
		<meta name="URL" content="<?php echo 'http://' . $server . $endereco; ?>">
		<meta name="robots" content="index, follow">
		<meta name="reply-to" content="">
		<meta name="classification" content="Internet">
		<meta name="distribution" content="Global">
		<meta name="rating" content="Safe for Kids">
		<meta name="theme-color" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		<link rel="canonical" href="<?php echo 'http://' . $server . $endereco; ?>" />
	    <meta property="og:locale" content="pt_BR" />
	    <meta property="og:type" content="website" />
	    <meta property="og:title" content="<?php echo $page_title; ?>" />
	    <meta property="og:description" content="" />
	    <meta property="og:url" content="<?php echo 'http://' . $server . $endereco; ?>" />
	    <meta property="og:site_name" content="<?php echo $page_title; ?>" />
	    <meta property="og:image" content="" />
	    <meta name="twitter:card" content="summary_large_image" />
	    <meta name="twitter:description" content="" />
	    <meta name="twitter:title" content="<?php echo $page_title; ?>" />
	    <meta name="twitter:image" content="" />

		<link rel="stylesheet" href="assets/css/style.min.css" media="screen">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
		<script src="assets/js/libs/respond.min.js"></script>
		<link rel="stylesheet" type="text/css" href="assets/css/ie8-style.css" media="screen" />
		<![endif]-->

	</head>
<body>
