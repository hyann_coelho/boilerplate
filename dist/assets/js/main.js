$(document).ready(function() {
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iphone|ipod|ipad|android|webos|iemobile|opera mini|blackberry)/);

    // Executando alguma coisa apenas em ambiente desktop
    if (!agentID) {
        // $(function() {
        //     $.stellar({
        //         horizontalScrolling: false,
        //         verticalOffset: 40,
        //         responsive: false,
        //     });
        // });
    }

    $("img[data-src], iframe[data-src]").each(function() {
        var src = $(this).attr('data-src');
        $(this).attr('src', src);
    });

    $("body").on('click', '.navbar-toggle', function() {
        $(".navbar-toggle").toggleClass('collapsed');
    });

    // // Scroll entre seções/dobras de uma pagina
    // $(".jumper").on('click', function(e) {
    //     e.preventDefault();
    //     var anchor = $(this).attr('href');
    //     $('html,body').animate({ scrollTop: $(anchor).offset().top }, 1000);
    // });

    // // Fechando bootstrap nav menu
    // $('.jumper').click(function() {
    //     $('.navbar-toggle:visible').click();
    // });


    // Formatando campos com mascaras
    var SPMaskBehavior = function(val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
    $(".mask-phone").mask(SPMaskBehavior, spOptions);
    $(".mask-date").mask('99/99/9999');
    $(".mask-cep").mask('00000-000');
    $(".mask-cpf").mask('000.000.000-00');
    $(".mask-cnpj").mask('00.000.000/0000-00');
    $('.mask-number').mask('0#');

    // Valida campos de formulário
    $(".contact-form").validate({
        errorPlacement: function (error, element) {
            return false;
        },
        rules: {
            nome: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            assunto: {
                required: true,
                minlength: 2
            },
            cidade: {
                required: true,
                minlength: 2
            },
            estado: {
                required: true
            },
            mensagem: {
                required: true,
                minlength: 2
            }
        },
        submitHandler: function(form) {
            ajaxRequest(".contact-form", 'envia.php');
        }
    });

    // Função de envio de formulários
    function ajaxRequest(form, url_r) {
        // Serializa todos os campos do formulário para type POST ou GET
        var dados = $(form).serialize();
        // Botão de enviar
        var btnSubmit = $(form).find(".btn-submit");
        // Recepiente de reposta do formulário
        var resultForm = $(form).find(".result-form");        
        // Caso o Botão de submit for um elemento <button>, utilizar .html() para capturar o conteúdo inicial
        var textInitialBtn = btnSubmit.val();
        // Texto do botão aguardo resposta Ajax
        var textSending = "Enviando...";
        // Enviado com sucesso
        var textSuccess = "Sua mensagem foi enviada com sucesso, em breve entraremos em contato.";
        // Erro no envio 
        var textError = "Erro ao enviar Mensagem!";

        $.ajax({
            type: "POST",
            url: url_r,
            data: dados,
            beforeSend: function() {
                btnSubmit.val(textSending);
                btnSubmit.attr('disabled', true);
            },
            success: function(data) {
                if (data == 1) {
                    $(form)[0].reset();
                    resultForm.removeClass("error");
                    resultForm.addClass("success").hide().html(textSuccess).fadeIn();
                    // window.location.href = "obrigado?nome=" + name;
                } else {
                    resultForm.removeClass("success");
                    resultForm.addClass("error").hide().html(textError).fadeIn();
                    console.log(data);
                }
            },
            error: function() {
                btnSubmit.val(textInitialBtn);
                btnSubmit.removeAttr('disabled');
                // console.log('erro no envio');
            },
            complete: function() {
                btnSubmit.val(textInitialBtn);
                btnSubmit.removeAttr('disabled');
                // console.log('completo');
            }
        });
    }

    // add suport placeholder ie9
    if (!Modernizr.input.placeholder) {
        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur();
        $('[placeholder]').parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
    }

});

function sistema() {
    if (navigator.userAgent.indexOf('Linux') != -1) {
        var so = "Linux";
    } else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('95') != -1)) {
        var so = "windows";
    } else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('98') != -1)) {
        var so = "windows";
    } else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('NT') != -1)) {
        var so = "windows";
    } else if ((navigator.userAgent.indexOf('Win') != -1) && (navigator.userAgent.indexOf('2000') != -1)) {
        var so = "windows";
    } else if (navigator.userAgent.indexOf('Mac') != -1) {
        var so = "macintosh";
    } else if (navigator.userAgent.toLowerCase().indexOf('unix') != -1) {
        var so = "Unix";
    } else {
        var so = "other";
    }

    return so;
}

if(sistema() !== 'macintosh'){
    //scroll suave
    $.smoothScroll();
}

// efeito scroll appear 
wow = new WOW({
  offset: 40
});
wow.init();
